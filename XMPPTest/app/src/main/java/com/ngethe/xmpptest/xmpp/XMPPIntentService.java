package com.ngethe.xmpptest.xmpp;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.chat2.OutgoingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jxmpp.jid.EntityBareJid;

import java.net.InetAddress;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class XMPPIntentService extends IntentService implements IncomingChatMessageListener, OutgoingChatMessageListener {
    public static final String TAG = XMPPIntentService.class.getSimpleName();

    // IntentService actions
    private static final String ACTION_CONNECT = "com.ngethe.xmpptest.action.ACTION_CONNECT";

    // ACTION_CONNECT parameters
    private static final String HOSTNAME = "com.ngethe.xmpptest.extra.hostaddress";
    private static final String USERNAME = "com.ngethe.xmpptest.extra.username";
    private static final String PASSWORD = "com.ngethe.xmpptest.extra.password";

    // XMPP message types
    public static final String TOAST = "toast";
    public static final String MESSAGE = "message";

    private final IBinder binder = new XMPPServiceBinder();
    private AbstractXMPPConnection xmppConnection;
    private ChatManager chatManager;

    public XMPPIntentService() {
        super("XMPPIntentService");
    }

    /**
     * Starts the XMPP service to connect to the XMPP server with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void connect(Context context, String host, String username, String password) {
        Intent intent = new Intent(context, XMPPIntentService.class);
        intent.setAction(ACTION_CONNECT);
        intent.putExtra(HOSTNAME, host);
        intent.putExtra(USERNAME, username);
        intent.putExtra(PASSWORD, password);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_CONNECT.equals(action)) {
                final String hostname = intent.getStringExtra(HOSTNAME);
                final String username = intent.getStringExtra(USERNAME);
                final String password = intent.getStringExtra(PASSWORD);
                handleActionConnect(hostname, username, password);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onDestroy() {
        if (xmppConnection != null) {
            chatManager.removeIncomingListener(this);
            chatManager.removeOutgoingListener(this);
            xmppConnection.disconnect();
        }
        Log.e(TAG, "Disconnected from XMPP server");
        super.onDestroy();
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionConnect(String hostname, String username, String password) {
        HostnameVerifier verifier = new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return false;
            }
        };
        try {
            InetAddress address = InetAddress.getByName(hostname);
            XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                    .setHost(hostname)
                    .setUsernameAndPassword(username, password)
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                    .allowEmptyOrNullUsernames()
                    .setXmppDomain(hostname)
                    .setHostnameVerifier(verifier)
                    .setHostAddress(address)
                    .setSendPresence(true)
                    .setCompressionEnabled(true)
                    .setPort(5222)
                    .build();
            xmppConnection = new XMPPTCPConnection(config);
            xmppConnection.connect();
            if (xmppConnection.isConnected()) {
                Log.e(TAG, "Connected to XMPP server");
            } else {
                Log.e(TAG, "Connection failed");
            }
            xmppConnection.login();
            if (xmppConnection.isAuthenticated()) {
                Log.e(TAG, "Authenticated");
                chatManager = ChatManager.getInstanceFor(xmppConnection);
                chatManager.addIncomingListener(this);
                chatManager.addOutgoingListener(this);

            } else {
                Log.e(TAG, "Authentication failed");
            }
        } catch (Exception e) {
            Log.e(TAG, "Error occurred while connecting to XMPP domain " + hostname);
            e.printStackTrace();
        }
    }

    @Override
    public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {
        Log.e(TAG, "Received new incoming message " + message.getBody());
    }

    @Override
    public void newOutgoingMessage(EntityBareJid to, Message message, Chat chat) {
        Log.e(TAG, "Sent new outgoing message " + message.getBody());
    }

    public class XMPPServiceBinder extends Binder {
        XMPPIntentService getService() {
            return XMPPIntentService.this;
        }
    }
}
