package com.ngethe.xmpptest.models;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Chat implements Serializable {
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private long _id;
    private String senderName;
    private String senderFullAddress;
    private int unreadMessagesCount;
    private Date modifiedAt;

    public Chat() {}

    public Chat(String senderName, String senderFullAddress) {
        this.senderName = senderName;
        this.senderFullAddress = senderFullAddress;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderFullAddress() {
        return senderFullAddress;
    }

    public void setSenderFullAddress(String senderFullAddress) {
        this.senderFullAddress = senderFullAddress;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public String getTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        return sdf.format(this.modifiedAt);
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public void setModifiedAt(String modifiedAt) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        try {
            this.modifiedAt = sdf.parse(modifiedAt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public int getUnreadMessagesCount() {
        return unreadMessagesCount;
    }

    public void setUnreadMessagesCount(int unreadMessagesCount) {
        this.unreadMessagesCount = unreadMessagesCount;
    }

    public void addUnreadMessages(int count) {
        this.unreadMessagesCount += count;
    }
}
