package com.ngethe.xmpptest;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ngethe.xmpptest.models.Chat;
import com.ngethe.xmpptest.models.Database;

import java.util.ArrayList;
import java.util.List;

public class ChatListAdapter extends BaseAdapter {
    private List<Chat> chatList = new ArrayList<>();
    private Context context;
    private Database database;

    public ChatListAdapter(Context context) {
        this.context = context;
        this.database = new Database(context);
        this.chatList = database.getChatsTable().all();
    }

    public void setContext(Context context) {
        this.context = context;
        if (context != null) {
            this.database = new Database(context);
            this.chatList = database.getChatsTable().all();
        } else {
            this.chatList = new ArrayList<>();
        }
        notifyDataSetChanged();
    }

    public void updateData() {
        if (database != null) {
            this.chatList = database.getChatsTable().all();
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return chatList.size();
    }

    @Override
    public Object getItem(int position) {
        return chatList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return chatList.get(position).get_id();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.card_chat, parent, false);
        }
        final Chat chat = chatList.get(position);
        TextView tv_name = convertView.findViewById(R.id.tv_name);
        tv_name.setText(chat.getSenderName());
        TextView tv_date = convertView.findViewById(R.id.tv_date);
        tv_date.setText(chat.getTimeStamp());
        TextView tv_latest_message = convertView.findViewById(R.id.tv_latest_message);

        TextView tv_unread_messages = convertView.findViewById(R.id.tv_unread_messages);
        tv_unread_messages.setText(String.valueOf(chat.getUnreadMessagesCount()));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra(ChatActivity.CHAT, chat);
                context.startActivity(intent);
            }
        });

        return convertView;
    }
}
