package com.ngethe.xmpptest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.ngethe.xmpptest.models.Chat;
import com.ngethe.xmpptest.models.ChatMessage;
import com.ngethe.xmpptest.xmpp.XMPPService;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String CHAT = "com.ngethe.xmpptest.extra.chat";

    private static final String TAG = ChatActivity.class.getSimpleName();


    private EditText txt_message;
    private MessageListAdapter messageListAdapter;
    private boolean boundToXmppService;

    Messenger xmppServiceMessenger;
    final Messenger xmppListenerMessenger = new Messenger(new ChatActivity.XMPPListenerHandler());

    String buddyFullAddress;
    String buddyName;

    Chat chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        txt_message = findViewById(R.id.txt_message);
        ImageButton btn_send_message = findViewById(R.id.btn_send_message);
        btn_send_message.setOnClickListener(this);

        chat = (Chat) getIntent().getSerializableExtra(CHAT);
        messageListAdapter = new MessageListAdapter(ChatActivity.this, chat);
        ListView messages_view = findViewById(R.id.messages_view);
        messages_view.setAdapter(messageListAdapter);


        bindToXMPPService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        messageListAdapter.update();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindFromXmppService();
    }

    private void bindToXMPPService() {
        Intent bindToServiceIntent = new Intent(this, XMPPService.class);
        bindService(bindToServiceIntent, xmppServiceConnection, Context.BIND_AUTO_CREATE);
        boundToXmppService = true;
    }

    private void unbindFromXmppService() {
        if (boundToXmppService) {
            if (xmppServiceMessenger != null ) {
                try {
                    Message msg = Message.obtain(null, XMPPService.REMOVE_LISTENER);
                    msg.replyTo = xmppListenerMessenger;
                    xmppServiceMessenger.send(msg);
                } catch (RemoteException ignored) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            unbindService(xmppServiceConnection);
            boundToXmppService = false;
        }
    }

    private void sendMessage(String sender, String message) {
        if (boundToXmppService) {
            if (xmppServiceMessenger != null ) {
                try {
                    Message msg = Message.obtain(null, XMPPService.OUTGOING_MESSAGE);
                    msg.replyTo = xmppListenerMessenger;
                    Bundle bundle = msg.getData();
                    bundle.putString(XMPPService.SENDER_FULL_ADDRESS, sender);
                    bundle.putString(XMPPService.MESSAGE, message);
                    xmppServiceMessenger.send(msg);
                } catch (RemoteException e) {
                    Log.e(TAG, "XMPPService seems to have crashed at some point");
                    e.printStackTrace();
                }
            }
        }

        ChatMessage sentChatMessage = new ChatMessage(message, sender, ChatMessage.SENT);
        sentChatMessage.setChatId(chat.get_id());
        messageListAdapter.addMessage(sentChatMessage);
    }

    private ServiceConnection xmppServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            xmppServiceMessenger = new Messenger(service);
            try {
                Message msg = Message.obtain(null, XMPPService.ADD_LISTENER);
                msg.replyTo = xmppListenerMessenger;
                xmppServiceMessenger.send(msg);
                Log.e(TAG, "Connected to service");
            } catch (RemoteException e) {
                Log.e(TAG, "XMPPService crashed before registering listener messenger");
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            xmppServiceMessenger = null;
            Log.e(TAG, "Deregistered listener from XMPP Service");
        }
    };

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_send_message) {
            String message = txt_message.getText().toString();
            sendMessage(chat.getSenderFullAddress(), message);
            txt_message.setText("");
        }
    }

    class XMPPListenerHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == XMPPService.INCOMING_MESSAGE) {
                handleIncomingMessage();
            }
        }

        public void handleIncomingMessage() {
            messageListAdapter.update();
        }
    }
}
